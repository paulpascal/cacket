# Cacket E-Commerce Project #

### What has be done so far? ###

* FOS (User + JSRouting + CkEditor) Installation & Configuration
* Code architecture set, from front-end to backend
* Backend: Controller, Entity, Forms, base templates for all resources

### How about the rest of the work? ###

* Amos: Front-end => Auth, Shop, Add to Cart Ajax Script
* Paul: done...
* Frejus: Blog Part => Handle blog part
* Code: You have to be obedient to your masters

### Other ###

* Please now that we have a basic code structure, i suggest
    us to work each other on separate branch from master for
    stability purpose
* I add a current final image dump of my databse in the structure [cacket_sym.sql](cacket_sym.sql) 
* :)   