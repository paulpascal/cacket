/**************************************************
 * This file is useful for payGate api consuming
 * PayGate Api Provider
 *
 ***************************************************/

var apiUrl = 'https://paygateglobal.com/api';

/**
 * Ask for a payment
 * @param paymentData {auth_token, phone_number, amount, ?description, identifier}
 * @return Promise of {tx_reference, status}
 * status:
 *          0 : Transaction enregistrée avec succès
 *          2 : Jeton d’authentification invalide
 *          4 : Paramètres Invalides
 *          6 : Doublons détectées. Une transaction avec le même identifiant existe déja.
 */
function askPayment(paymentData) {
    return $.ajax({
        url: apiUrl + '/v1/pay',
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        data: paymentData
    })
}

/**
 * @desc Check payment status
 * @param paymentData {auth_token, tx_reference}
 * @return Promise of
 */
function checkPayment1(paymentData) {
    return $.ajax({
        url: apiUrl + '/v1/status',
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        data: paymentData
    })
}

/**
 * @desc Check payment status
 * @param paymentData {auth_token, identifier}
 * @return Promise of
 */
function checkPayment2(paymentData) {
    return $.ajax({
        url: apiUrl + '/v2/status',
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        data: paymentData
    })
}