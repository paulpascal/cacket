<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CouponRepository")
 */
class Coupon
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Command", inversedBy="coupons")
     * @ORM\JoinColumn(nullable=true)
     */
    private $command;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isConsumed;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $consumedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $genList;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isConsumed = false;
        $this->consumedAt = null;
        $this->command = null;
        $this->uid = $this->generateUid();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCommand(): ?Command
    {
        return $this->command;
    }

    public function setCommand(?Command $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function getIsConsumed(): ?bool
    {
        return $this->isConsumed;
    }

    public function setIsConsumed(bool $isConsumed): self
    {
        $this->isConsumed = $isConsumed;
        $this->setConsumedAt(new \DateTime());
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getConsumedAt(): ?\DateTimeInterface
    {
        return $this->consumedAt;
    }

    public function setConsumedAt(\DateTimeInterface $consumedAt): self
    {
        $this->consumedAt = $consumedAt;

        return $this;
    }

    /**
     * @param int $code_length
     * @return string
     */
    function generateUid($code_length = 6) {
        $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $code = "CAKT";
        for ($i = 0; $i < $code_length; $i++)
        {
            $code .= $characters[mt_rand(0, strlen($characters)-1)];
        }
        return $code;
    }

    public function getGenList(): ?string
    {
        return $this->genList;
    }

    public function setGenList(string $genList): self
    {
        $this->genList = $genList;

        return $this;
    }
}
