<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSub;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Media", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $media;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryPromotion", mappedBy="category", cascade={"remove"})
     */
    private $categoryPromotions;

    public function __construct()
    {
        $this->createdAt = new  \DateTime();
        $this->promotions = new ArrayCollection();
        $this->categoryPromotions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsSub(): ?bool
    {
        return $this->isSub;
    }

    public function setIsSub(bool $isSub): self
    {
        $this->isSub = $isSub;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getMedia(): ?Media
    {
        return $this->media;
    }

    public function setMedia(?Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return Collection|CategoryPromotion[]
     */
    public function getCategoryPromotions(): Collection
    {
        return $this->categoryPromotions;
    }

    public function addCategoryPromotion(CategoryPromotion $categoryPromotion): self
    {
        if (!$this->categoryPromotions->contains($categoryPromotion)) {
            $this->categoryPromotions[] = $categoryPromotion;
            $categoryPromotion->setCategory($this);
        }

        return $this;
    }

    public function removeCategoryPromotion(CategoryPromotion $categoryPromotion): self
    {
        if ($this->categoryPromotions->contains($categoryPromotion)) {
            $this->categoryPromotions->removeElement($categoryPromotion);
            // set the owning side to null (unless already changed)
            if ($categoryPromotion->getCategory() === $this) {
                $categoryPromotion->setCategory(null);
            }
        }

        return $this;
    }
}
