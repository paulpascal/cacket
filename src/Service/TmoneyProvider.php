<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 1/23/19
 * Time: 11:46 AM
 */

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Permet de consomer l'api de paiment Tmoney
 * Class TmoneyProvider
 * @package App\Service
 */
class TmoneyProvider
{
    private $container;
    private $mailer;
    private $httpClient = null;

    const API_URL = 'https://22808.tagpay.fr/online/online.php';
    const MERCHANT_ID = '2896303167249049';

    public function __construct(ContainerInterface $container, \Swift_Mailer $mailer)
    {
        $this->container = $container;
        $this->httpClient = new Client();
    }

    public function askSession()
    {
        try {
            $response = $this->httpClient->request('GET',
                self::API_URL . '?merchantid=' . self::MERCHANT_ID
            );
            return $response->getBody();
        }
        catch (GuzzleException $e) {
            return null;
        }
    }

    public function askTMoneyPayment()
    {

    }
}