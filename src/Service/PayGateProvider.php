<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 1/23/19
 * Time: 11:46 AM
 */

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Permet de consomer l'api de paiment Paygate global
 * Class PayGateProvider
 * @package App\Service
 */
class PayGateProvider
{
    private $container;
    private $mailer;
    private $httpClient = null;

    const API_URL = 'https://paygateglobal.com/api';
    const API_KEY = 'd45269dd-33e1-4d0a-8833-a6bfa9426f32';

    public function __construct(ContainerInterface $container, \Swift_Mailer $mailer)
    {
        $this->container = $container;
        $this->mailer = $mailer;
        $this->httpClient = new Client();
    }

    public function askFloozPayment($data)
    {
        $data['auth_token'] = self::API_KEY;
        try {
            $response = $this->httpClient->request('POST',
                self::API_URL . '/v1/pay',
                [
                    'headers' => array('Content-Type' => 'application/json'),
                    'body' => json_encode($data)
                ]
            );
            return \GuzzleHttp\json_decode($response->getBody(), true);
        }
        catch (GuzzleException $e) {
            return null;
        }
    }

    public function checkFloozPayment($identifier)
    {
        $data['auth_token'] = self::API_KEY;
        $data['identifier'] = $identifier;
        try {
            $response = $this->httpClient->request('POST',
                self::API_URL . '/v2/status',
                [
                    'headers' => array('Content-Type' => 'application/json'),
                    'body' => json_encode($data)
                ]
            );
            return \GuzzleHttp\json_decode($response->getBody(), true);
        }
        catch (GuzzleException $e) {
            var_dump($e->getMessage());
            return null;
        }
    }

}