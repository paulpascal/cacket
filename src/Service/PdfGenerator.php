<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 1/23/19
 * Time: 11:46 AM
 */

namespace App\Service;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Permet de sauver le fichier dans un repertoire donné au préalable enregistrer dans paramaters
 * et retourne le nouveau nom généré du fichier copier
 * Class FileSaver
 * @package App\Service
 */
class PdfGenerator
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function generatePdf($filename, $html)
    {
        return new PdfResponse(
            $this->container->get('knp_snappy.pdf')->getOutputFromHtml($html),
            $filename
        );
    }

}