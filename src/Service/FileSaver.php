<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 1/23/19
 * Time: 11:46 AM
 */

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Permet de sauver le fichier dans un repertoire donné au préalable enregistrer dans paramaters
 * et retourne le nouveau nom généré du fichier copier
 * Class FileSaver
 * @package AdminBundle\Service
 */
class FileSaver
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function saveFile(File $file, $directory)
    {
        // Generate a unique name for the file before saving it
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();

        // Move the file to the directory where brochures are stored
        $file->move(
            $this->container->getParameter($directory), $fileName
        );

        return $fileName;
    }
}