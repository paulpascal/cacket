<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 1/23/19
 * Time: 11:46 AM
 */

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Permet d'envoyer des mails
 * Class MailSender
 * @package App\Service
 */
class MailSender
{
    private $container;
    private $mailer;

    public function __construct(ContainerInterface $container, \Swift_Mailer $mailer)
    {
        $this->container = $container;
        $this->mailer = $mailer;
    }

    public function send($html, $options)
    {
        $message = (new \Swift_Message($options['subject']))
            ->setFrom($options['sender'])
            ->setTo($options['recipient'])
            ->setBody(
                $html,
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'emails/registration.txt.twig',
                    ['name' => $name]
                ),
                'text/plain'
            )
            */
        ;

        // Troubles ? Check: https://swiftmailer.symfony.com/docs/sending.html

        return $this->mailer->send($message);
    }
}