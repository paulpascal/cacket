<?php

namespace App\Repository;

use App\Entity\CategoryPromotion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CategoryPromotion|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryPromotion|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryPromotion[]    findAll()
 * @method CategoryPromotion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryPromotionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoryPromotion::class);
    }

    // /**
    //  * @return CategoryPromotion[] Returns an array of CategoryPromotion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryPromotion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
