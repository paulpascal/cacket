<?php

namespace App\Form;

use App\Entity\Post;
use App\Entity\PostCategory;
use App\Entity\PostTag;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, array(
                'class' => PostCategory::class,
                'choice_label' => 'name',
                'placeholder' => 'Selectionner'
            ))
            ->add('title')
            ->add('content', CKEditorType::class)
            ->add('media', MediaType::class)
            ->add('tags', EntityType::class, array(
                'class' => PostTag::class,
                'choice_label' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control select2', 'style' => 'width: 100%'),
                'placeholder' => 'Selectionner',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
