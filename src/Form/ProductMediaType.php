<?php

namespace App\Form;

use App\Entity\ProductMedia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductMediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('media', MediaType::class, array(
                'label_attr' => array('class' => 'hide_label'),
                'attr' => array('class' => 'with_hidden_label')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductMedia::class,
        ]);
    }
}
