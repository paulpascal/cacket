<?php

namespace App\Form;

use App\Entity\PostCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', TextareaType::class)
            ->add('media', MediaType::class, array(
                'required' => false
            ))
            ->add('isSub', CheckboxType::class, array(
                'label' => 'Est-ce une sous-catégorie ?',
                'required' => false
            ))
            ->add('parent', EntityType::class, array(
                'class' => PostCategory::class,
                'choice_label' => 'name',
                'required' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PostCategory::class,
        ]);
    }
}
