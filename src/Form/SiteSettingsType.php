<?php

namespace App\Form;

use App\Entity\Settings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('siteTitle', TextType::class, array('label' => 'Titre du site', 'required' => false))
            ->add('tel',TelType::class, array('label' => 'Tel', 'required' => false))
            ->add('email', EmailType::class, array('label' => 'Email du site', 'required' => false))
            ->add('logo', MediaType::class, array('label' => 'Logo du site', 'required' => false))
            ->add('facebookUrl', TextType::class, array('required' => false))
            ->add('instaUrl', TextType::class, array('required' => false))
            ->add('twitterUrl', TextType::class, array('required' => false))
            ->add('siteDescription', TextareaType::class, array('required' => false))
            ->add('siteAddress', TextareaType::class, array('required' => false))
            ->add('maintenance',CheckboxType::class,
                array('label' => 'Mettre le site en maintenance', 'required' => false)
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Settings::class,
        ]);
    }
}
