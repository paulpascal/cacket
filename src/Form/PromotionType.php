<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Promotion;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PromotionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate')
            ->add('endDate')
            ->add('percent')
            ->add('products', EntityType::class, array(
                'class' => Product::class,
                'choice_label' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control select2', 'style' => 'width: 100%'),
                'placeholder' => 'Selectionner',
            ))
            ->add('categories', EntityType::class, array(
                'class' => Category::class,
                'choice_label' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control select2', 'style' => 'width: 100%'),
                'placeholder' => 'Selectionner'
            ))
        ;
    }
}
