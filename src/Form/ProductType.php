<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductMedia;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'choice_label' => 'name',
                'placeholder' => 'Selectionner'
            ))
            ->add('name')
            ->add('price')
            ->add('stock')
            ->add('description', TextareaType::class)
            ->add('featured', CheckboxType::class, array(
                'required' => false
            ))
            ->add('media', MediaType::class)
            ->add('medias', CollectionType::class, array(
                'entry_type' => ProductMediaType::class,
                'allow_add' => TRUE,
                'allow_delete' => TRUE,
                'prototype' => true,
                'by_reference' => false ,
                'attr' => array('class' => 'p-l-5'),
                'label_attr' => array('class' => 'font-weight-bold'),
                'entry_options' => array(
                    'attr' => array('class' => 'col-9'),
                    'label_attr' => array('class' => 'text-center card-header'),
                ),
                'required' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
