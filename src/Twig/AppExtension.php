<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 9/20/18
 * Time: 11:13 AM
 *
 * Cette classe contiendra les methodes et filtres redéfini qui seront accessible dans toute l'application en global
 */

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return array(
            // La logique de cette fonction est maintenant implémenté dans une classe différente (AppRuntime)
            new TwigFunction('productsCount', array(AppRuntime::class, 'getProductsCount')),
            new TwigFunction('categoriesCount', array(AppRuntime::class, 'getCategoriesCount')),
            new TwigFunction('commandsCount', array(AppRuntime::class, 'getCommandsCount')),
            new TwigFunction('customersCount', array(AppRuntime::class, 'getCustomersCount')),
            new TwigFunction('settings', array(AppRuntime::class, 'getSettings')),
            new TwigFunction('cart', array(AppRuntime::class, 'cart')),
        );
    }
}