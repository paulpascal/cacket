<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 9/20/18
 * Time: 11:56 AM
 *
 * Cette classe contient toutes les implémentations des fonctions/Filtres/variables accessible en global
 */

namespace App\Twig;

use App\Entity\Settings;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime  implements RuntimeExtensionInterface
{
    private $em;
    private $session;

    /**
     * AppRuntime constructor.
     * @param EntityManagerInterface $em
     * @param SessionInterface $session
     */
    public function __construct(EntityManagerInterface $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * Get products count
     * @return int
     */
    public function getProductsCount()
    {
        return $this->em->getRepository('App:Product')->count([
        ]);
    }

    /**
     * Get categories count
     * @return int
     */
    public function getCategoriesCount()
    {
        return $this->em->getRepository('App:Category')->count([
        ]);
    }

    /**
     * Get commands count
     * @return int
     */
    public function getCommandsCount()
    {
        return $this->em->getRepository('App:Command')->count([
        ]);
    }

    /**
     * Get customers count
     * @return int
     */
    public function getCustomersCount()
    {
        return $this->em->getRepository('App:User')->count([
            'roles' => ["a:0:{}"]
        ]);
    }

    public function cart()
    {
        $cart = [];
        if ($this->session->has('cart'))
            $cart = $this->session->get('cart');
        return $cart;
    }

    /**
     * Get settings
     * @return Settings|null
     */
    public function getSettings()
    {
        return $this->em->find('App:Settings', 1);
    }
}