<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190329180219 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category_promotion (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, promotion_id INT DEFAULT NULL, INDEX IDX_67DAFE112469DE2 (category_id), INDEX IDX_67DAFE1139DF194 (promotion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_promotion (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, promotion_id INT DEFAULT NULL, INDEX IDX_AFBDCB5C4584665A (product_id), INDEX IDX_AFBDCB5C139DF194 (promotion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_promotion ADD CONSTRAINT FK_67DAFE112469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category_promotion ADD CONSTRAINT FK_67DAFE1139DF194 FOREIGN KEY (promotion_id) REFERENCES promotion (id)');
        $this->addSql('ALTER TABLE product_promotion ADD CONSTRAINT FK_AFBDCB5C4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_promotion ADD CONSTRAINT FK_AFBDCB5C139DF194 FOREIGN KEY (promotion_id) REFERENCES promotion (id)');
        $this->addSql('ALTER TABLE promotion DROP FOREIGN KEY FK_C11D7DD112469DE2');
        $this->addSql('ALTER TABLE promotion DROP FOREIGN KEY FK_C11D7DD14584665A');
        $this->addSql('DROP INDEX IDX_C11D7DD112469DE2 ON promotion');
        $this->addSql('DROP INDEX IDX_C11D7DD14584665A ON promotion');
        $this->addSql('ALTER TABLE promotion ADD start_date DATETIME DEFAULT NULL, ADD end_date DATETIME DEFAULT NULL, DROP product_id, DROP category_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE category_promotion');
        $this->addSql('DROP TABLE product_promotion');
        $this->addSql('ALTER TABLE promotion ADD product_id INT DEFAULT NULL, ADD category_id INT DEFAULT NULL, DROP start_date, DROP end_date');
        $this->addSql('ALTER TABLE promotion ADD CONSTRAINT FK_C11D7DD112469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE promotion ADD CONSTRAINT FK_C11D7DD14584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_C11D7DD112469DE2 ON promotion (category_id)');
        $this->addSql('CREATE INDEX IDX_C11D7DD14584665A ON promotion (product_id)');
    }
}
