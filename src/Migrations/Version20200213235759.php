<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200213235759 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, command_id INT NOT NULL, amount NUMERIC(10, 2) NOT NULL, currency VARCHAR(10) DEFAULT NULL, client_name VARCHAR(50) DEFAULT NULL, client_mobile VARCHAR(20) DEFAULT NULL, payment_ref VARCHAR(20) NOT NULL, payment_id VARCHAR(20) DEFAULT NULL, timestamp VARCHAR(50) DEFAULT NULL, ip_addr VARCHAR(20) DEFAULT NULL, status VARCHAR(10) DEFAULT NULL, error VARCHAR(50) DEFAULT NULL, client_id_from_api VARCHAR(10) DEFAULT NULL, INDEX IDX_723705D133E1689A (command_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D133E1689A FOREIGN KEY (command_id) REFERENCES command (id)');
        $this->addSql('ALTER TABLE command ADD declined TINYINT(1) DEFAULT NULL, ADD declined_at DATETIME DEFAULT NULL, ADD canceled TINYINT(1) DEFAULT NULL, ADD canceled_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE transaction');
        $this->addSql('ALTER TABLE command DROP declined, DROP declined_at, DROP canceled, DROP canceled_at');
    }
}
