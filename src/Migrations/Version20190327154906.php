<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190327154906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE settings ADD logo_id INT DEFAULT NULL, DROP logo');
        $this->addSql('ALTER TABLE settings ADD CONSTRAINT FK_E545A0C5F98F144A FOREIGN KEY (logo_id) REFERENCES media (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E545A0C5F98F144A ON settings (logo_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE settings DROP FOREIGN KEY FK_E545A0C5F98F144A');
        $this->addSql('DROP INDEX UNIQ_E545A0C5F98F144A ON settings');
        $this->addSql('ALTER TABLE settings ADD logo VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP logo_id');
    }
}
