<?php

namespace App\Controller\Blog;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends Controller
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index()
    {
        return $this->render('blog/index.html.twig');
    }


    /**
     * @Route("/blog/post/{id}", name="blog_post", options={"expose"=true})
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blogShow(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $post->increaseViews();
        $em->flush();

        return $this->render('blog/show.html.twig', [
            'post' => $post
        ]);
    }
}