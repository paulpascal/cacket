<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home", options={"expose"=true})
     */
    public function index()
    {
        return $this->redirectToRoute('shop');
    }

    /**
     * @Route("/back_office", name="back_office")
     */
    public function backOffice()
    {
        return $this->redirectToRoute('back_office_home');
    }
}
