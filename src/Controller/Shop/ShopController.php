<?php

namespace App\Controller\Shop;

use App\Entity\Category;
use App\Entity\Command;
use App\Entity\CommandLine;
use App\Entity\Product;
use App\Entity\Transaction;
use App\Entity\User;
use App\Form\TrackingType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ShopController extends Controller
{
    /**
     * @Route("/shop/", name="shop", options={"expose"=true})
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $topProducts = $em->getRepository('App:Product')->findBy(
            array('featured' => true)
        );
        $allProducts = $em->getRepository('App:Product')->findBy(
            [], ['id' => 'desc']
        );
        return $this->render('shop/index.html.twig', [
            'topProducts' => $topProducts,
            'allProducts' => $allProducts,
        ]);
    }

    /**
     * @Route("/shop/contact", name="shop_contact", options={"expose"=true})
     */
    public function contact()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('shop/contact.html.twig', [
        ]);
    }

    /**
     * @Route("shop/products/from/{id}", name="shop_products", defaults={"id"=null}, options={"expose"=true})
     * @param Category|null $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productsByCategory(?Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $supCategories = $em->getRepository('App:Category')->findBy([

        ], [], 15);

        $products = array();

        if ($category !== null) {
            $products = $em->getRepository('App:Product')->findBy(
                ['category' => $category->getId()], ['createdAt' => 'DESC']
            );
        }
        else {
            $products = $em->getRepository('App:Product')->findBy(
                [], ['createdAt' => 'DESC'], 10
            );
        }
        return $this->render('shop/category.html.twig', [
            'products' => $products,
            'supCategories' => $supCategories,
            'category' => $category
        ]);
    }

    /**
     * @Route("/shop/product/{id}/detail", name="shop_product_detail", options={"expose"=true})
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productDetail(Product $product)
    {
        $em = $this->getDoctrine()->getManager();
        $topProducts = $em->getRepository('App:Product')->findBy(
            array('featured' => true)
        );
        return $this->render('shop/product_detail.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/shop/cart", name="shop_cart", options={"expose"=true})
     */
    public function cart()
    {
        $cart = [];
        $cartDetail = [];
        $price = 0;

        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        if ($session->has('cart'))
            $cart = $session->get('cart');

        foreach ($cart as $productId => $n)
        {
            $product = $em->find('App:Product', $productId);
            if ($product != null)
            {
                $price += $product->getPrice() * $n;

                $cartDetail[] = array(
                    $product, $n
                );
            }
        }

        return $this->render('shop/cart.html.twig', array(
            'cart' => $cartDetail,
            'total' => $price,
        ));
    }

    /**
     * @Route("/shop/tracking", name="shop_tracking", options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function tracking(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER'))
            return $this->redirectToRoute('shop_commands');

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(TrackingType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $uid = $form->get('uid')->getData();
            $command = $em->getRepository('App:Command')->findOneBy(array(
                'uid' => $uid
            ));
            if ($command)
                return $this->redirectToRoute('shop_checkout_status', array('id' => $command->getId()));
            $this->addFlash('tracking', 'Aucune commande portant cet ID n\'a été trouvé.');
        }

        return $this->render('shop/tracking.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/shop/commands", name="shop_commands", options={"expose"=true})
     */
    public function commands()
    {
        $em = $this->getDoctrine()->getManager();
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER'))
            return $this->redirectToRoute('shop_tracking');

        $commands = $em->getRepository('App:Command')->findBy(
            array('customer' => $this->getUser())
        );
        return $this->render('shop/command.html.twig', [
            'commands' => $commands,
        ]);
    }

    /**
     * @Route("/shop/checkout", name="shop_checkout", options={"expose"=true})
     */
    public function checkout()
    {
        $cartToken = null;
        $price = 0;

        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');

        if ($session->has('cart')) {
            $cart = $session->get('cart');
            $cartToken = $session->get('cartToken');

            /** @var Command $command */
            $command = $em->getRepository('App:Command')->findOneByToken($cartToken);

            if ($command == null) {
                $command = new Command();
                if ($this->getUser())
                    $command->setCustomer($this->getUser());
                $em->persist($command);
            } else {
                foreach ($command->getLines() as $line) {
                    $command->removeLine($line);
                }
            }

            foreach ($cart as $productId => $n) {
                $product = $em->find('App:Product', $productId);
                $price += $product->getPrice() * $n;

                $commandLine = new CommandLine();
                $commandLine->setProduct($product);
                $commandLine->setQty($n);
                $commandLine->setCommand($command);
                $em->persist($commandLine);

                $command->addLine($commandLine);
            }

            $command->setAmount($price);
            $command->setToken($cartToken);
            $em->flush();

            $this->evaluateCommand($command);

            $handle = curl_init();
            $url = "https://22808.tagpay.fr/online/online.php?merchantid=2896303167249049";
            // Set the url
            curl_setopt($handle, CURLOPT_URL, $url);
            // Set the result output to be a string.
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($handle);
            curl_close($handle);
            $sessionId = str_replace('OK:', '', $output);

            $payGateUrl = str_replace('api', '', $this->get('paygate_client')::API_URL)
                . 'v1/page?token=' . $this->get('paygate_client')::API_KEY
                . '&amount=' . $command->getAmount()
                . '&description=Paiement de votre commande [ ' . $command->getUid() . ' ] sur CACKET'
                . '&identifier='
                . $command->getUid()
                . '&url=http://cakket.com/public/shop/flooz/verify/'
                . $command->getUid();


            return $this->render('/shop/checkout.html.twig', [
                'command' => $command,
                'tmoneyMerchantId' => $this->get('tmoney_client')::MERCHANT_ID,
                'tmoneySessionId' => $sessionId,
                'payGateUrl' => $payGateUrl,
            ]);
        }
        else {
            return $this->redirectToRoute('shop_cart');
        }
    }

    /**
     * @Route("/shop/process/flooz_payment/{id}", name="shop_process_flooz_payment", options={"expose"=true})
     * @param Request $request
     * @return Response
     */
    public function askFloozPayment(Request $request, Command $command) {
        $em = $this->getDoctrine()->getManager();
        $command->setMethod('FLOOZ');

        //
        /*$data['phone_number'] = '79629053';
        $data['amount'] = $this->evaluateCommand($command);
        $data['description'] = 'Paiement d\'une commande sur CACKET';
        $data['identifier'] = $command->getUid();
        //
        $resp = $this->get('paygate_client')->askFloozPayment($data);

        $em->flush();
        if ($resp == null) return new Response();
        else {
            if ($resp['tx_reference'] == -1)
            {
                $err = [
                    0 => 'Transaction enregistrée avec succès',
                    2 => 'Jeton d’authentification invalide',
                    4 => 'Paramètres Invalides',
                    6 => 'Doublons détectées. Une transaction avec le même identifiant existe déja.',
                ];
                return $this->render('shop/checkout.html.twig', array(
                    'command' => $command,
                    'err' => $err[$resp['status']]
                ));
            }
            else {
                $txRef = $resp['tx_reference'];
                $command->setTxRef($txRef);
                // Checking the status of a payment
                return new JsonResponse($resp);
            }
        }*/
        //
        $url = str_replace('api', '', $this->get('paygate_client')::API_URL)
            . 'v1/page?token=' . $this->get('paygate_client')::API_KEY
            . '&amount=' . $this->evaluateCommand($command)
            . '&description=Paiement d\'une commande sur CACKET'
            . '&identifier='
            . $command->getUid()
            . '&url=https://cacket.com/shop/process/tmoney_payment/'
            . $command->getUid();

        $em->flush();
        return $this->redirect($url);
    }

    /**
     * @Route("/shop/process/tmoney_payment/{id}", name="shop_process_tmoney_payment", options={"expose"=true})
     * @param Request $request
     * @param Command $command
     * @return Response
     */
    public function askTmoneyPayment(Request $request, Command $command) {
        $em = $this->getDoctrine()->getManager();
        $command->setMethod('TMONEY');

        //
        $url = str_replace('api', '', $this->get('paygate_client')::API_URL)
                    . 'v1/page?token=' . $this->get('paygate_client')::API_KEY
                    . '&amount=' . $this->evaluateCommand($command)
                    . '&description=Paiement d\'une commande sur CACKET'
                    . '&identifier='
                    . $command->getUid()
                    . '&url=https://cacket.com/shop/process/tmoney_payment/'
                    . $command->getUid();

        $em->flush();
        return $this->redirect($url);
    }

    /**
     * @Route("/shop/payment/paygate/result/on/{uid}", name="shop_handle_paygate_response")
     * @param Request $request
     * @return Response
     */
    public function receivePaymentResponse(Request $request, $uid)
    {
        $em = $this->getDoctrine()->getManager();
        $command = $em->getRepository('App:Command')->findOneBy([
            'uid' => $uid
        ]);

        if ($command != null) {
            $data  = $request->request->all();
            dump($data);
            return new Response();
        }
        return $this->json(array('msg' => 'Unknown requuest. Any of our command has this identifier'), 200);
    }


    /**
     * @Route("/shop/checkout/{id}/status", name="shop_checkout_status", options={"expose"=true})
     * @param Command $command
     * @return Response
     */
    public function checkoutConfirmation(Command $command)
    {
        return $this->render('shop/checkout_verified.html.twig', array(
           'command' => $command
        ));
    }

    /**
     * @Route("/shop/add/product/{id}/{n}/time/to/cart", name="shop_add_to_cart", options={"expose"=true})
     * @param Product $product
     * @param $n
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addToCartAction(Product $product, $n)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');

        if (!$session->has('cart')){
            $session->set('cart', []);
            $cartToken = bin2hex($this->generateRandomString(8));
            $session->set('cartToken', $cartToken);
        }
        $cart = $session->get('cart');

        // First check stock
        $currentStock = $product->getStock();
        if ($currentStock >= $n)
        {
            $cart[$product->getId()] = $n;
            $session->set('cart', $cart);
            $product->setStock($currentStock - $n);
            $em->flush();
            return $this->json(array('response' => 'OK', 'data' => $cart));
        }
        else
        {
            return $this->json(
                array(
                    'response' => 'ERROR',
                    'data' => $cart,
                    'message' => 'Stock insuffisant', 'stock' => $currentStock
                ), 400
            );
        }
    }

    /**
     * @Route("/shop/remove/{id}/from/cart", name="shop_remove_from_cart", options={"expose"=true})
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function removeFromCartAction(Product $product)
    {
        $session = $this->get('session');

        if (!$session->has('cart')) {
            $session->set('cart', []);
            $cartToken = bin2hex($this->generateRandomString(8));
            $session->set('cartToken', $cartToken);
        }

        $cart = $session->get('cart');
        $newCart = [];

        foreach ($cart as $id => $n)
        {
            if ($id != $product->getId())
                $newCart[$id] = $n;
        }

        $session->set('cart', $newCart);
        return $this->json(array('response' => 'OK', 'data' => $newCart));
    }

    /**
     * @Route("/shop/update/product/{id}/by/{newQty}/in/cart", name="shop_update_cart", options={"expose"=true})
     * @param Product $product
     * @param $newQty
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateCartAction(Product $product, $newQty)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');

        if (!$session->has('cart')) {
            $session->set('cart', []);
            $cartToken = bin2hex($this->generateRandomString(8));
            $session->set('cartToken', $cartToken);
        }
        $cart = $session->get('cart');

        foreach ($cart as $id => $n) {
            if ($id == $product->getId()) {
                // previous qty
                $prevQty = $cart[$id];

                $currentStock = $product->getStock();
                if ($currentStock + $prevQty >= $newQty) {
                    $cart[$id] = $newQty;
                    $session->set('cart', $cart);
                    $product->setStock($currentStock + $prevQty - $newQty);
                    $em->flush();
                    return $this->json(array('response' => 'OK', 'data' => $cart));
                } else {
                    return $this->json(
                        array(
                            'response' => 'ERROR',
                            'data' => $cart,
                            'message' => 'Stock insuffisant', 'stock' => $currentStock + $prevQty
                        ), 400
                    );
                }
            }

            return $this->json(array('response' => 'OK', 'data' => $cart));
        }

        $session->set('cart', $cart);
        return $this->json(array('response' => 'OK', 'data' => $cart));
    }

    /**
     * @Route("/verify", name="shop_payment_notification", options={"expose"=true})
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyCommandPayment(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $purchaseref = $request->query->get('purchaseref');
        $amount = $request->query->get('amount');
        $currency = $request->query->get('currency');
        $status = $request->query->get('status');
        $clientid = $request->query->get('clientid');
        $cname = $request->query->get('cname');
        $mobile = $request->query->get('mobile');
        $paymentref = $request->query->get('paymentref');
        $payid = $request->query->get('payid');
        $timestamp = $request->query->get('timestamp');
        $ipaddr = $request->query->get('ipaddr');
        $error = $request->query->get('error');

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setCurrency($currency);
        $transaction->setClientIdFromApi($clientid);
        $transaction->setClientName($cname);
        $transaction->setClientMobile($mobile);
        $transaction->setPaymentRef($paymentref);
        $transaction->setPaymentId($payid);
        $transaction->setTimestamp($timestamp);
        $transaction->setIpAddr($ipaddr);
        $transaction->setError($error);

        $command = $em->getRepository('App:Command')->findOneBy(
            array('uid' => $purchaseref)
        );
        if ($command) {
            if ($status && $status === 'OK') {
                $command->setTxRef($paymentref);
                $command->setPaid(true);
            }
            $transaction->setCommand($command);
        }
        $em->persist($transaction);
        $em->flush();

        return $this->json(array('response' => 'OK'));
    }

    /**
     * @Route("/shop/flooz/verify/{identif}", name="shop_fpayment_notification", options={"expose"=true})
     * @param Request $request
     * @param $identif
     * @return Response
     * @throws \Exception
     */
    public function verifyFloozCommandPayment(Request $request, $identif)
    {
        $em = $this->getDoctrine()->getManager();
        $command = $em->getRepository('App:Command')->findOneBy(
            array('uid' => $identif)
        );
        $msg = 'Le paiement est en attente! Veuillez confirmer le paiement svp';

        if ($request->isMethod('POST')) {

            $tx_reference = $request->request->get('tx_reference');
            $payment_reference = $request->request->get('payment_reference');
            $amount = $request->request->get('amount');
            $datetime = $request->request->get('datetime');
            $payment_method = $request->request->get('payment_method');
            $phone_number = $request->request->get('phone_number');


            $transaction = new Transaction();
            $transaction->setAmount($amount);
            $transaction->setClientMobile($phone_number);
            $transaction->setPaymentRef($payment_reference);
            $transaction->setPaymentId($tx_reference);
            $transaction->setTimestamp($datetime);


            if ($command) {
                $command->setTxRef($payment_reference);

                $command->setPaid(true);
                $command->setMethod($payment_method);
                $transaction->setCommand($command);
                $msg = 'Le paiement a été effectué avec succès!';

            }
            $em->persist($transaction);
            $em->flush();

            $this->get('session')->remove('cart');
            $this->get('session')->remove('cartToken');
        }
        else {
            $resp = $this->get('paygate_client')->checkFloozPayment($identif);

            $tx_reference = $resp['tx_reference'];
            $payment_reference = $resp['payment_reference'];
            $datetime = $resp['datetime'];
            $payment_method = $resp['payment_method'];
            $status = $resp['status'];

            $transaction = new Transaction();
            $transaction->setAmount($command->getAmount());
            $transaction->setPaymentRef($payment_reference);
            $transaction->setPaymentId($tx_reference);
            $transaction->setTimestamp($datetime);

            if ($command) {
                if ($status) {

                    $this->get('session')->remove('cart');
                    $this->get('session')->remove('cartToken');

                    if ($status == 0) {
                        $command->setTxRef($payment_reference);
                        $command->setPaid(true);
                    } else if ($status == 4) {
                        $command->setDeclined(true);
                        $command->setPaid(false);
                        $command->setDeclinedAt(new \DateTime());
                        $msg = 'Le paiement a expiré! Veuillez recommencer svp';

                        $this->restoreCommandProductStock($command);
                    } else if ($status == 6) {
                        $command->setCanceled(true);
                        $command->setPaid(false);
                        $command->setCanceledAt(new \DateTime());
                        $msg = 'Le paiement a été! Veuillez recommencer svp';

                        $this->restoreCommandProductStock($command);
                    }
                    $command->setMethod($payment_method);
                }
                $transaction->setCommand($command);
                $em->persist($transaction);
                $em->flush();
            }
        }

        return $this->render('shop/checkout_verified.html.twig', array(
            'command' => $command,
            'msg' => $msg
        ));
    }

    /**
     * @Route("/shop/check/{id}/confirmed", name="shop_confirm_payment", options={"expose"=true})
     * @param Request $request
     * @param Command $command
     * @return Response
     * @throws \Exception
     */
    public function confirmCommandPayment(Request $request, Command $command)
    {
        $em = $this->getDoctrine()->getManager();

        $command->setMethod('TMONEY');
        $command->setPaidAt(new \DateTime());
        $command->setPaid(true);

        $this->get('session')->remove('cart');
        $this->get('session')->remove('cartToken');

        $em->flush();

        return $this->render('shop/checkout_verified.html.twig', array(
            'command' => $command,
            'msg' => 'Paiement confirmé avec succès.'
        ));
    }

    /**
     * @Route("/shop/check/{id}/declined", name="shop_decline_payment", options={"expose"=true})
     * @param Request $request
     * @param Command $command
     * @return Response
     * @throws \Exception
     */
    public function declineCommandPayment(Request $request, Command $command)
    {
        $em = $this->getDoctrine()->getManager();

        $command->setMethod('TMONEY');
        $command->setDeclined(true);
        $command->setDeclinedAt(new \DateTime());
        $this->restoreCommandProductStock($command);

        $em->flush();

        $this->get('session')->remove('cart');
        $this->get('session')->remove('cartToken');

        return $this->render('shop/checkout_verified.html.twig', array(
            'command' => $command,
            'msg' => 'Le paiement a été refusé.'
        ));
    }

    /**
     * @Route("/shop/check/{id}/canceled", name="shop_cancel_payment", options={"expose"=true})
     * @param Request $request
     * @param Command $command
     * @return Response
     * @throws \Exception
     */
    public function cancelCommandPayment(Request $request, Command $command)
    {
        $em = $this->getDoctrine()->getManager();

        $command->setMethod('TMONEY');
        $command->setCanceled(true);
        $command->setCanceledAt(new \DateTime());
        $this->restoreCommandProductStock($command);

        $em->flush();

        $this->get('session')->remove('cart');
        $this->get('session')->remove('cartToken');

        return $this->render('shop/checkout_verified.html.twig', array(
            'command' => $command,
            'msg' => 'Le paiement a été annulé.'
        ));
    }

    /**
     * @Route("shop/ajax/create_user/for/{id}", name="shop_ajax_create_user", options={"expose"=true})
     * @param Request $request
     * @param Command $command
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxCreateUser(Request $request, Command $command)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $session = $this->get('session');
            $cart = null;
            $cartToken = null;
            if ($session->has('cart')) {
                $cart = $session->get('cart');
                $cartToken = $session->get('cartToken');
            }

            $nom = $data['nom'];
            $prenoms = $data['prenoms'];
            $tel = $data['tel'];
            $email = $data['email'];
            $city = $data['city'];
            $quarter = $data['quarter'];

            // Create user here
            $user = new User();
            $user->setEmail($email);
            $user->setLastName($nom);
            $user->setFirstName($prenoms);
            $user->setTel($tel);
            $user->setAddress($city . '  ' . $quarter);
            $user->setPlainPassword($email);
            $user->setEnabled(true);
            $em->persist($user);
            $command->setCustomer($user);
            $em->flush();

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            $this->get('session')->set('cart', $cart);
            $this->get('session')->set('cartToken', $cartToken);
            $event = new InteractiveLoginEvent($request, $token);
            $this->get('event_dispatcher')->dispatch('security.interactive_login', $event);

            return $this->json(array('response' => 'OK', 'data' => $user));
        } catch (UniqueConstraintViolationException $exception) {
            return $this->json( array('response' => 'NOK', 'data' => 'Cette adresse email est déjà utilisé. Connectez-vous plutôt.'), 400);
        }
    }

    /**
     * @Route("shop/ajax/update_command/{id}", name="shop_ajax_update_command", options={"expose"=true})
     * @param Request $request
     * @param Command $command
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxUpdateCommand(Request $request, Command $command)
    {

        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        $nom = $data['nom'];
        $prenoms = $data['prenoms'];
        $tel = $data['tel'];
        $email = $data['email'];
        $city = $data['city'];
        $quarter = $data['quarter'];


        $command->setCustomer(null);
        $command->setCustomerName($nom . ' ' .$prenoms);
        $command->setCustomerEmail($email);
        $command->setCustomerTel($tel);
        $command->setCustomerAddress($city . ' ' . $quarter);
        $em->flush();

        return $this->json(array('response' => 'OK', 'data' => 'OK Update'));

    }

    /**
     * @Route("shop/ajax/apply_coupon/for/{id}", name="shop_ajax_apply_coupon", options={"expose"=true})
     * @param Request $request
     * @param Command $command
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxApplyCoupon(Request $request, Command $command)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();

            $couponId = $data['coupon'];
            $coupon = $em->getRepository('App:Coupon')->findOneBy(array(
               'uid' => $couponId
            ));
            if ($coupon) {
                if ($coupon->getIsConsumed()) {
                    return $this->json(array('response' => 'NOK', 'data' => 'Ce coupon est déjà consomé.'), 400);
                } else {
                    $command->addCoupon($coupon);
                    $this->evaluateCommand($command);
                    $coupon->setIsConsumed(true);
                    return $this->json(array('response' => 'OK', 'data' => 'Coupon valide'));
                }
            } else {
                return $this->json(array('response' => 'NOK', 'data' => 'Ce coupon n\'existe pas.'), 400);
            }

        } catch (UniqueConstraintViolationException $exception) {
            return $this->json( array('response' => 'NOK', 'data' => 'Cette adresse email est déjà utilisé. Connectez-vous plutôt.'), 400);
        }
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Evaluate a passed command object by adding contained products price and applying
     * related coupon(s) if possible
     * @param Command $command
     * @return float|int|null
     */
    function evaluateCommand(Command $command) {
        $totalPrice = 0;

        // Evaluate by products
        foreach ($command->getLines() as  $commandLine) {
            $totalPrice += $commandLine->getProduct()->getPrice() * $commandLine->getQty();
        }
        // Evaluate by coupon(s)
        foreach ($command->getCoupons() as $coupon) {
            if (!$coupon->getIsConsumed()) {
                $totalPrice -= $coupon->getAmount();
            }
        }

        $command->setAmount($totalPrice);
        $this->getDoctrine()->getManager()->flush();

        return $totalPrice;
    }

    public function restoreCommandProductStock(Command $command)
    {
        // Evaluate by products
        foreach ($command->getLines() as  $commandLine) {
            $commandLine->getProduct()->incrementStock($commandLine->getQty());
        }
        $this->getDoctrine()->getManager()->flush();
    }
}
