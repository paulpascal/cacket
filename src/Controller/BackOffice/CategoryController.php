<?php

namespace App\Controller\BackOffice;

use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends Controller
{
    /**
     * @Route("/back_office/category", name="back_office_category")
     * @param Request $request
     * @return Response
     */
    public function indexCategoryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('App:Category')->findAll();

        return $this->render('back_office/category/index.html.twig', array(
            'categories' => $categories
        ));
    }

    /**
     * @Route("/back_office/category/add", name="back_office_add_category")
     * @param Request $request
     * @return Response
     */
    public function addCategoryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            if ($category->getMedia())
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $category->getMedia()->getFile(), 'media_directory'
                );
                $category->getMedia()->setFileName($fileName);
            }

            $em->persist($category);
            $em->flush();
            $this->addFlash('notification', 'Catégorie ajoutée avec succès!');
            return  $this->redirectToRoute('back_office_category');
        }

        return $this->render('back_office/category/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/category/{id}/edit", name="back_office_edit_category")
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function editCategoryAction(Request $request, Category $category)
    {
        $em = $this->getDoctrine()->getManager();

        if ($category->getMedia())
            $category->getMedia()->setFile(
                new File($this->getParameter('media_directory').'/'. $category->getMedia()->getFileName())
            );

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            if ($category->getMedia() !== null && $category->getMedia()->getFile() !== null)
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $category->getMedia()->getFile(), 'media_directory'
                );
                $category->getMedia()->setFileName($fileName);
            }

            $em->persist($category);
            $em->flush();
            $this->addFlash('notification', 'Catégorie modifiée avec succès!');
            return $this->redirectToRoute('back_office_category');
        }


        return $this->render('back_office/category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/back_office/category/{id}/delete", name="back_office_delete_category")
     * @param Category $category
     * @return Response
     */
    public function deleteCategoryAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();
        $this->addFlash('notification', 'Catégorie supprimée avec succès!');
        return $this->redirectToRoute('back_office_category');
    }
}
