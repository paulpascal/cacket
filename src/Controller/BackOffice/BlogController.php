<?php

namespace App\Controller\BackOffice;

use App\Entity\Post;
use App\Entity\PostTag;
use App\Entity\PostCategory;
use App\Form\PostCategoryType;
use App\Form\PostTagType;
use App\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends Controller
{
    /**
     * @Route("/back_office/blog/category", name="back_office_blog_category")
     * @param Request $request
     * @return Response
     */
    public function indexCategoryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('App:PostCategory')->findAll();

        return $this->render('back_office/blog/category/index.html.twig', array(
            'categories' => $categories
        ));
    }

    /**
     * @Route("/back_office/blog/add/category", name="back_office_add_blog_category")
     * @param Request $request
     * @return Response
     */
    public function addCategoryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = new PostCategory();
        $form = $this->createForm(PostCategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            if ($category->getMedia())
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $category->getMedia()->getFile(), 'media_directory'
                );
                $category->getMedia()->setFileName($fileName);
            }

            $em->persist($category);
            $em->flush();
            $this->addFlash('notification', 'Catégorie ajoutée avec succès!');
            return  $this->redirectToRoute('back_office_blog_category');
        }

        return $this->render('back_office/blog/category/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/blog/category/{id}/edit", name="back_office_edit_blog_category")
     * @param Request $request
     * @param PostCategory $category
     * @return Response
     */
    public function editCategoryAction(Request $request, PostCategory $category)
    {
        $em = $this->getDoctrine()->getManager();

        if ($category->getMedia())
            $category->getMedia()->setFile(
                new File($this->getParameter('media_directory').'/'. $category->getMedia()->getFileName())
            );

        $form = $this->createForm(PostCategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            if ($category->getMedia() !== null && $category->getMedia()->getFile() !== null)
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $category->getMedia()->getFile(), 'media_directory'
                );
                $category->getMedia()->setFileName($fileName);
            }

            $em->persist($category);
            $em->flush();
            $this->addFlash('notification', 'Catégorie modifiée avec succès!');
            return $this->redirectToRoute('back_office_blog_category');
        }


        return $this->render('back_office/blog/category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/back_office/blog/category/{id}/delete", name="back_office_delete_blog_category")
     * @param PostCategory $category
     * @return Response
     */
    public function deleteCategoryAction(PostCategory $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();
        $this->addFlash('notification', 'Catégorie supprimée avec succès!');
        return $this->redirectToRoute('back_office_blog_category');
    }



    /**
     * @Route("/back_office/blog/post", name="back_office_blog_post")
     * @param Request $request
     * @return Response
     */
    public function indexPostAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('App:Post')->findAll();

        return $this->render('back_office/blog/post/index.html.twig', array(
            'posts' => $posts
        ));
    }

    /**
     * @Route("/back_office/blog/show/post/{id}", name="back_office_show_blog_post")
     * @param Post $post
     * @return Response
     */
    public function showPostAction(Post $post)
    {
        return $this->render('back_office/blog/post/show.html.twig', array(
            'post' => $post
        ));
    }

    /**
     * @Route("/back_office/blog/add/post", name="back_office_add_blog_post")
     * @param Request $request
     * @return Response
     */
    public function addPostAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            if ($post->getMedia())
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $post->getMedia()->getFile(), 'media_directory'
                );
                $post->getMedia()->setFileName($fileName);
            }

            $em->persist($post);
            $em->flush();
            $this->addFlash('notification', 'Article ajouté avec succès!');
            return  $this->redirectToRoute('back_office_blog_post');
        }

        return $this->render('back_office/blog/post/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/blog/post/{id}/edit", name="back_office_edit_blog_post")
     * @param Request $request
     * @param Post $post
     * @return Response
     */
    public function editPostAction(Request $request, Post $post)
    {
        $em = $this->getDoctrine()->getManager();

        if ($post->getMedia())
            $post->getMedia()->setFile(
                new File($this->getParameter('media_directory').'/'. $post->getMedia()->getFileName())
            );

        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            if ($post->getMedia()->getFile() !== null)
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $post->getMedia()->getFile(), 'media_directory'
                );
                $post->getMedia()->setFileName($fileName);
            }

            $em->persist($post);
            $em->flush();
            $this->addFlash('notification', 'Article modifié avec succès!');
            return $this->redirectToRoute('back_office_blog_post');
        }


        return $this->render('back_office/blog/post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/back_office/blog/post/{id}/delete", name="back_office_delete_blog_post")
     * @param Post $post
     * @return Response
     */
    public function deletePostAction(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('notification', 'Article supprimé avec succès!');
        return $this->redirectToRoute('back_office_blog_post');
    }



    /**
     * @Route("/back_office/blog/tag", name="back_office_blog_tag")
     * @param Request $request
     * @return Response
     */
    public function indexTagAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tags = $em->getRepository('App:PostTag')->findAll();

        return $this->render('back_office/blog/tag/index.html.twig', array(
            'tags' => $tags
        ));
    }

    /**
     * @Route("/back_office/blog/add/tag", name="back_office_add_blog_tag")
     * @param Request $request
     * @return Response
     */
    public function addTagAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tag = new PostTag();
        $form = $this->createForm(PostTagType::class, $tag);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $em->persist($tag);
            $em->flush();
            $this->addFlash('notification', 'Etiquette ajouté avec succès!');
            return  $this->redirectToRoute('back_office_blog_tag');
        }

        return $this->render('back_office/blog/tag/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/blog/tag/{id}/edit", name="back_office_edit_blog_tag")
     * @param Request $request
     * @param PostTag $tag
     * @return Response
     */
    public function editTagAction(Request $request, PostTag $tag)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(PostTagType::class, $tag);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($tag);
            $em->flush();
            $this->addFlash('notification', 'Etiquette modifié avec succès!');
            return $this->redirectToRoute('back_office_blog_tag');
        }


        return $this->render('back_office/blog/tag/edit.html.twig', [
            'tag' => $tag,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/back_office/blog/tag/{id}/delete", name="back_office_delete_blog_tag")
     * @param PostTag $tag
     * @return Response
     */
    public function deleteTagAction(PostTag $tag)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($tag);
        $em->flush();
        $this->addFlash('notification', 'Etiquette supprimé avec succès!');
        return $this->redirectToRoute('back_office_blog_post');
    }
}
