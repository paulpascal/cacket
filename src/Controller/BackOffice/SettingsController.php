<?php

namespace App\Controller\BackOffice;


use App\Entity\CategoryPromotion;
use App\Entity\ProductPromotion;
use App\Entity\Promotion;
use App\Entity\Settings;
use App\Form\PromotionType;
use App\Form\SiteSettingsType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SettingsController extends Controller
{
    /**
     * @Route("/back_office/settings/slide", name="back_office_settings_slide")
     * @param Request $request
     * @return void
     */
    public function slideAction(Request $request) {

    }

    /**
     * @Route("/back_office/settings/site", name="back_office_settings_site")
     * @param Request $request
     * @return Response
     */
    public function siteSettingsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $siteSettings = $em->find('App:Settings', 1);
        if ($siteSettings == null) { $siteSettings = new Settings(); $em->persist($siteSettings); }

        $settingsForm = $this->createForm(SiteSettingsType::class,  $siteSettings);
        $settingsForm->handleRequest($request);

        if ($settingsForm->isSubmitted() && $settingsForm->isValid())
        {
            if ($siteSettings->getLogo()->getFile() !== null)
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $siteSettings->getLogo()->getFile(), 'media_directory'
                );
                $siteSettings->getLogo()->setFileName($fileName);
            }
            $em->flush();
            $this->addFlash('notification', 'Paramètres du site enrégistrés avec succès!');
        }

        return $this->render('back_office/settings/site.html.twig', array(
            'form' => $settingsForm->createView(),
            'siteSettings' => $siteSettings
        ));
    }

    /**
     * @Route("/back_office/settings/promotion", name="back_office_promotion")
     * @param Request $request
     * @return Response
     */
    public function promotionSettingsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $promotions = $em->getRepository('App:Promotion')->findAll();

        return $this->render('back_office/settings/promotion_list.html.twig', array(
            'promotions' => $promotions
        ));
    }

    /**
     * @Route("/back_office/settings/promotion/add", name="back_office_add_promotion")
     * @param Request $request
     * @return Response
     */
    public function addPromotionSettingsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(PromotionType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $data = $form->getData();
            $startDate = $data['startDate'];
            $endDate = $data['startDate'];
            $percent = $data['percent'];
            /** @var ArrayCollection $products */
            $products = $data['products'];
            /** @var ArrayCollection $categories */
            $categories = $data['categories'];

            $promotion = new Promotion();
            $promotion->setPercent($percent);
            $promotion->setStartDate(\DateTime::createFromFormat("d/m/Y", $startDate));
            $promotion->setEndDate(\DateTime::createFromFormat("d/m/Y", $endDate));
            if ($products->count() == 0) {
                foreach ($categories as $category) {
                    $categoryPromotion = new CategoryPromotion();
                    $categoryPromotion->setCategory($category);
                    $categoryPromotion->setPromotion($promotion);
                    $em->persist($categoryPromotion);
                    $em->flush();
                    $this->addFlash('notification', 'Promotion crée avec succès');
                    return $this->redirectToRoute('back_office_promotion');
                }
            }
            else {
                foreach ($products as $product) {
                    $productPromotion = new ProductPromotion();
                    $productPromotion->setProduct($product);
                    $productPromotion->setPromotion($promotion);
                    $em->persist($productPromotion);
                    $em->flush();
                    $this->addFlash('notification', 'Promotion crée avec succès');
                    return $this->redirectToRoute('back_office_promotion');
                }
            }

        }
        return $this->render('back_office/settings/promotion.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/settings/promotion/{id}/edit", name="back_office_edit_promotion")
     * @param Request $request
     * @param Promotion $promotion
     * @return Response
     */
    public function editPromotionSettingsAction(Request $request, Promotion $promotion) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(PromotionType::class);

        $form->get('startDate')->setData($promotion->getStartDate()->format('d/m/Y'));
        $form->get('endDate')->setData($promotion->getEndDate()->format('d/m/Y'));
        $form->get('percent')->setData($promotion->getPercent());
        $form->get('categories')->setData($promotion->getCategoryPromotions());
        $form->get('products')->setData($promotion->getProductPromotions());

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $data = $form->getData();
            $startDate = $data['startDate'];
            $endDate = $data['startDate'];
            $percent = $data['percent'];
            $products = $data['products'];
            $categories = $data['categories'];

            $promotion = new Promotion();
            $promotion->setPercent($percent);
            $promotion->setStartDate(\DateTime::createFromFormat("d/m/Y", $startDate));
            $promotion->setEndDate(\DateTime::createFromFormat("d/m/Y", $endDate));
            if ($products == null) {
                foreach ($categories as $category) {
                    $categoryPromotion = new CategoryPromotion();
                    $categoryPromotion->setCategory($category);
                    $categoryPromotion->setPromotion($promotion);
                    $em->persist($categoryPromotion);
                    $this->addFlash('notification', 'Promotion modifiée avec succès');
                    $em->flush();
                    return $this->redirectToRoute('back_office_promotion');
                }
            }
            else {
                foreach ($products as $product) {
                    $productPromotion = new ProductPromotion();
                    $productPromotion->setProduct($product);
                    $productPromotion->setPromotion($promotion);
                    $em->persist($productPromotion);
                    $this->addFlash('notification', 'Promotion modifiée avec succès');
                    $em->flush();
                    return $this->redirectToRoute('back_office_promotion');
                }
            }
        }
        return $this->render('back_office/settings/promotion.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/settings/promotion/{id}/show", name="back_office_show_promotion")
     * @param Promotion $promotion
     * @return Response
     */
    public function showPromotionSettingsAction(Promotion $promotion) {
        return $this->render('back_office/settings/promotion_show.html.twig', array(
            'promotion' => $promotion
        ));
    }

    /**
     * @Route("/back_office/settings/promotion/{id}/delete", name="back_office_delete_promotion")
     * @param Promotion $promotion
     * @return Response
     */
    public function deletePromotionSettingsAction(Promotion $promotion) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($promotion);
        $this->addFlash('notification', 'Promotion supprimée avec succès!');
        return $this->redirectToRoute('back_office_promotion');
    }
}
