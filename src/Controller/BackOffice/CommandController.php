<?php

namespace App\Controller\BackOffice;

use App\Entity\Category;
use App\Entity\Command;
use App\Entity\Media;
use App\Entity\Product;
use App\Entity\ProductMedia;
use App\Form\CategoryType;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommandController extends Controller
{
    /**
     * @Route("/back_office/command/{paid}", name="back_office_command")
     * @param Request $request
     * @param null $paid
     * @return Response
     */
    public function indexCommandAction(Request $request, $paid = null)
    {
        $em =  $this->getDoctrine()->getManager();
        $criteria =  array();
        if ($paid != null)
            $criteria['paid'] = $paid;

        $purchases = $em->getRepository('App:Command')->findBy($criteria, array(
            'createdAt' => 'DESC'
        ));

        return $this->render('back_office/purchases/index.html.twig', [
            'purchases' => $purchases,
            'paid' => $paid
        ]);
    }

    /**
     * @Route("/back_office/command/{id}/show", name="back_office_show_command")
     * @param Command $command
     * @return Response
     */
    public function showCommandAction(Command $command)
    {
        return $this->render('back_office/purchases/show.html.twig', [
            'purchase' => $command,
        ]);
    }

    /**
     * @Route("/back_office/command/{id}/delete", name="back_office_delete_command")
     * @param Command $command
     * @return Response
     */
    public function deleteCommandAction(Command $command)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($command);
        $em->flush();
        return $this->redirectToRoute('back_office_command');
    }

}
