<?php

namespace App\Controller\BackOffice;

use App\Entity\Category;
use App\Entity\Command;
use App\Entity\Media;
use App\Entity\Product;
use App\Entity\ProductMedia;
use App\Form\CategoryType;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{

    /**
     * @Route("/back_office/customer", name="back_office_customer")
     * @param Request $request
     * @return Response
     */
    public function indexCustomersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $allAsers = $em->getRepository('App:User')->findAll();

        $clients = [];
        /** @var User $user */
        foreach ($allAsers as $user) {
            if (!$user->hasRole('ROLE_SUPER_ADMIN')) $clients[] = $user;
        }

        return $this->render('back_office/users/index.html.twig', [
            'customers' => $clients
        ]);
    }

    /**
     * @Route("/back_office/customer/{id}/command/{paid}", name="back_office_customer_commands")
     * @param Request $request
     * @param User $customer
     * @param null $paid
     * @return Response
     */
    public function customerCommandsAction(Request $request, User $customer, $paid = null)
    {
        return $this->render('default/admin.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/back_office/user", name="back_office_user")
     * @param Request $request
     * @return Response
     */
    public function indexUsersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findBy([
            'roles' =>  ["a:0:{}"]
        ]);

        return $this->render('@Admin/users/index.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("/back_office/user/{id}/show", name="back_office_show_user")
     * @param User $user
     * @return Response
     */
    public function userDetailAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $userCommands = $em->getRepository('App:Command')->findBy(array(
            'customer' => $user
        ), array('createdAt' => 'DESC'));

        return $this->render('back_office/users/show.html.twig',array(
            'user' => $user,
            'userPurchases' => $userCommands
        ));
    }
}
