<?php

namespace App\Controller\BackOffice;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductMedia;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends Controller
{
    /**
     * @Route("/back_office/product", name="back_office_product")
     * @param Request $request
     * @return Response
     */
    public function indexProductAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('App:Product')->findAll();

        return $this->render('back_office/product/index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * @Route("/back_office/product/top", name="back_office_top_product")
     * @param Request $request
     * @return Response
     */
    public function topProductAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $topCategories = $em->getRepository('App:Category')->getTop();
        $products = array();
        /** @var Category $category */
        foreach ($topCategories as $category)
        {
            $products[$category->getName()] = $em->getRepository('App:Product')
                                                ->getTop($category->getId());
        }

        return $this->render('back_office/product/index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * @Route("/back_office/product/by_category/{id}", name="back_office_product_by_category")
     * @param Request $request
     * @return Response
     */
    public function indexProductByCategoryAction(Request $request, Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('App:Product')->findBy([
            'category' => $category->getId()
        ]);

        return $this->render('back_office/product/index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * @Route("/back_office/product/add", name="back_office_add_product")
     * @param Request $request
     * @return Response
     */
    public function addProductAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            if ($product->getMedia()->getFile() !== null)
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $product->getMedia()->getFile(), 'media_directory'
                );
                $product->getMedia()->setFileName($fileName);
            }

            /** @var ProductMedia $otherFile */
            if ($product->getMedias())
                foreach ($product->getMedias() as $productMedia) {
                    $fileName = $this->get('file_saver')->saveFile(
                        $productMedia->getMedia()->getFile(), 'media_directory'
                    );
                    $productMedia->getMedia()->setFileName($fileName);
                }


            $em->persist($product);
            $em->flush();
            return $this->redirectToRoute('back_office_product');
        }

        return $this->render('back_office/product/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/product/{id}/edit", name="back_office_edit_product")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function editProductAction(Request $request, Product $product)
    {
        $em = $this->getDoctrine()->getManager();

        if ($product->getMedia())
            $product->getMedia()->setFile(
                new File($this->getParameter('media_directory').'/'. $product->getMedia()->getFileName())
            );

        $productMedias = $this->getDoctrine()->getManager()
            ->getRepository('App:ProductMedia')
            ->findBy(array('product' => $product));

        $form = $this->createForm(ProductType::class, $product);


        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($product->getMedia()->getFile() !== null)
            {
                $fileName = $this->get('file_saver')->saveFile(
                    $product->getMedia()->getFile(), 'media_directory'
                );
                $product->getMedia()->setFileName($fileName);
            }


            /** @var ProductMedia $otherFile */
            if ($product->getMedias())
                foreach ($product->getMedias() as $productMedia) {
                    $fileName = $this->get('file_saver')->saveFile(
                        $productMedia->getMedia()->getFile(), 'media_directory'
                    );
                    $productMedia->getMedia()->setFileName($fileName);
                }

            $em->flush();
            return $this->redirectToRoute('back_office_product');
        }

        return $this->render('back_office/product/edit.html.twig', [
            'product' => $product,
            'productMedias' => $productMedias,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/back_office/product/{id}/show", name="back_office_show_product")
     * @param Product $product
     * @return Response
     */
    public function showProductAction(Product $product)
    {
//        $productMedias = $this->getDoctrine()->getManager()
//                             ->getRepository('App:ProductMedia')
//                            ->findBy(array('product' => $product));

        return $this->render('back_office/product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/back_office/product/{id}/delete", name="back_office_delete_product")
     * @param Product $product
     * @return Response
     */
    public function deleteProductAction(Product $product)
    {
        $em = $this->getDoctrine()->getManager();
        $commandLines = $em->getRepository('App:CommandLine')->findBy(
            array('product' => $product->getId())
        );
        foreach ($commandLines as $commandLine) {
            $em->remove($commandLine);
        }
        $em->remove($product);
        $em->flush();
        return $this->redirectToRoute('back_office_product');
    }

}
