<?php

namespace App\Controller\BackOffice;

use App\Entity\Category;
use App\Entity\Command;
use App\Entity\Media;
use App\Entity\Product;
use App\Entity\ProductMedia;
use App\Form\CategoryType;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeController extends Controller
{
    /**
     * @Route("/back_office/", name="back_office_home")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('back_office/index.html.twig');
    }
}
