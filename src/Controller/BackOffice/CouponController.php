<?php

namespace App\Controller\BackOffice;


use App\Entity\Coupon;
use App\Form\CouponType;
use App\Form\GenCouponType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CouponController extends Controller
{
    /**
     * @Route("/back_office/coupon", name="back_office_coupon")
     * @param Request $request
     * @return Response
     */
    public function indexCouponAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $coupons = $em->getRepository('App:Coupon')->getGenList();
        return $this->render('back_office/coupon/index.html.twig', array(
            'coupons' => $coupons
        ));
    }

    /**
     * @Route("/back_office/coupon/add", name="back_office_add_coupon")
     * @param Request $request
     * @return Response
     */
    public function addCouponAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(GenCouponType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            $nb = $form->get('nb')->getData();
            $genList = (new \DateTime())->getTimestamp();

            for ($i = 0; $i < $nb; $i++) {
                $coupon = new Coupon();
                $coupon->setGenList($genList);
                $coupon->setAmount($form->get('amount')->getData());
                $em->persist($coupon);
            }

            $em->flush();
            $this->addFlash('notification', 'Coupon(s) ajouté(s) avec succès!');
            return  $this->redirectToRoute('back_office_coupon');
        }

        return $this->render('back_office/coupon/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back_office/coupon/{id}/edit", name="back_office_edit_coupon")
     * @param Request $request
     * @param Coupon $coupon
     * @return Response
     */
    public function editCouponAction(Request $request, Coupon $coupon)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CouponType::class, $coupon);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->flush();
            $this->addFlash('notification', 'Coupon modifié avec succès!');
            return $this->redirectToRoute('back_office_coupon');
        }


        return $this->render('back_office/coupon/edit.html.twig', [
            'coupon' => $coupon,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/back_office/coupon/{genId}/show", name="back_office_show_coupon")
     * @param $genId
     * @return Response
     */
    public function showCouponAction($genId)
    {
        $em = $this->getDoctrine()->getManager();
        $coupons = $em->getRepository('App:Coupon')->findBy([
            'genList' => $genId
        ]);

        return $this->render('back_office/coupon/show.html.twig', [
            'coupons' => $coupons,
            'genId' => $genId
        ]);
    }

    /**
     * @Route("/back_office/coupon/{id}/delete", name="back_office_delete_coupon")
     * @param Coupon $coupon
     * @return Response
     */
    public function deleteCouponAction(Coupon $coupon)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($coupon);
        $em->flush();
        $this->addFlash('notification', 'Coupon supprimé avec succès!');
        return $this->redirectToRoute('back_office_coupon');
    }

    /**
     * @Route("/back_office/coupon/{genId}/print", name="back_office_print_coupon")
     * @param $genId
     * @return Response
     */
    public function printCouponAction($genId) {
        $em = $this->getDoctrine()->getManager();
        $coupons = $em->getRepository('App:Coupon')->findBy([
            'genList' => $genId,
            'isConsumed' => false
        ]);

        $html = $this->renderView('back_office/coupon/print.html.twig', [
            'coupons' => $coupons,
            'genId' => $genId
        ]);

        $filename = 'coupon' . $genId  . '.pdf';
        return $this->get('pdf_generator')->generatePdf($filename, $html);
    }
}
